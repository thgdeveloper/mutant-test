# Build Setup


## `Important` 
This application depends to elasticsearch connection starts , then , is very important to follow the order of steps .
 - 1. Create a elasticsearch container and run it
 - 2. Create an application container and run it
 
 When the application starts it will call `wait-for-elasticsearch.sh` to check if the elasticsearch connection is ready , then will execute `npm start` command


## ````Running without Docker````

1 .Installing dependencies
```
npm install 
```

2 .Executing project locally
```
npm start
```





----------


## ````Running with Docker````

### 1. Bulding ElasticSearch Image
```
docker.elastic.co/elasticsearch/elasticsearch:6.7.2

```

### 2. Executing ElasticSearch Container
```
docker run --restart=always -d --name elasticsearch -p 9200:9200 -e “http.host=0.0.0.0” -e “transport.host=127.0.0.1” docker.elastic.co/elasticsearch/elasticsearch:6.7.2
```

### 3. Build Application Image

```
docker build -t mutant-test .
```


### 4. Executing Application Container
```
docker build -t mutant-test .
```

## `Challenge steps and routes`

### 1. -  Os websites de todos os usuários
```    
http://localhost:8080/websites
```

### 2. -  Os websites de todos os usuários
```    
http://localhost:8080/users
```

### 3. Mostrar todos os usuários que no endereço contem a palavra ```suite```
```    
http://localhost:8080/suits
```

### 4. Salvar logs de todas interações no elasticsearch
```    
see http://localhost:9200/logs/_search?size=1000
```


## `Utils commands`

### See running docker containers
```    
docker ps
```
### See docker images
```    
docker images
```
### Kill all running container process
```    
docker kill $(docker ps -q)
```



    



