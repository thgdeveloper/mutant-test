const express = require('express');
const app = express();

(function startRoutesV2(){
    const Controller = require('./app/api/controller');
    const Controllers = new Controller();
    const router = require('./app/api/route');
    const routes = router(express.Router(),Controllers);
    app.use(routes);
})();

module.exports = app;
