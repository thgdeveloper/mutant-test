const BaseService = require('../../base/base_service');

class JsonPlaceholderService extends BaseService {
    constructor() {
        super();
        this.baseUrl = 'https://jsonplaceholder.typicode.com/users';
    }

    getUsersJson() {
        return new Promise((resolve, reject) => {
            const options = {
                method: 'GET',
                url: this.baseUrl,
                json: true,
            };

            this.requestPromise.get(options)
                .then((response) => {
                    const responseBody = this.formatResponse(response);
                    resolve(responseBody);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

module.exports = JsonPlaceholderService;