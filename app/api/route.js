'use strict';

module.exports = function (router, Controller) {

    //METHOD::GET
    router.get('/health-check', (req, res) => {
        res.status(200).json({});
    });
    
    router.get('/',(req,res,next) => Controller.index(req,res,next));
    router.get('/websites',(req,res,next) => Controller.websites(req,res,next));
    router.get('/users',(req,res,next) => Controller.getUsers(req,res,next));
    router.get('/suits',(req,res,next) => Controller.getUserSuitAddress(req,res,next));

    return router;
};

