const elasticsearch = require('elasticsearch');

class ElasticSearch{
  constructor(){
    this.client = new elasticsearch.Client({
      host: 'http://172.17.0.2:9200'
    });

    this.ping();
    // this.dropIndex('logs');
    this.createIndex('logs');
  }

   dropIndex(index) {
    return this.client.indices.delete({
      index,
    });
  }

  log(obj){
      return new Promise((resolve, reject) => {
          obj.date = new Date();
        return this.addToIndex('logs',obj,'logs');
      })
  }

   addToIndex(index,body,type) {

    return this.client.index({
      index,
      type,
      body
    });
  }

  ping(){
    this.client.ping({
        requestTimeout: 30000,
      },  (error, res) => {
        if (error) console.error('------------------ ERROR RUN ELASTICSEARCH ------------------');
      
        console.info('------------------ RUN ELASTICSEARCH ------------------');
        console.info(`HOST: http://localhost:9200`);
      });
  }

  async createIndex(index){
    try {
        if(await this.exists(index)) return false;

        const body = {
          "mappings": {
            "logs":{
              "properties":{
                "success": {
                  "type": "boolean"
                },
                "description": {
                  "type": "text"
                },
                "date": {
                  "type": "date"
                },
              }
            }
          }
        }
    return this.client.indices.create({
      "index": index,
        body
    });

    } catch (error) {
        console.log(`Error trying to create index ${index}`);
        return false;
    }
  }

  async  exists(index) {
    let exist = false;
    try {
      return await this.client.indices.exists({ index });
    } catch (e) {
      console.log(e.message)
    }
    return exist;
  }
}

module.exports = ElasticSearch;