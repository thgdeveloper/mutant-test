'use strict';
const HandleResponse = require('../utils/handle_response');
const BaseBusiness = require('../base/business');
const JsonPlaceHolderService = require('./../integrations/json-placeholder-service');

class Business extends BaseBusiness {
    constructor() {
        super();
        this.handleResponse = new HandleResponse();
        this.jsonPlaceHolderService = new JsonPlaceHolderService();
    }

    getUsers(){
        return new Promise((resolve, reject) => {
            this.jsonPlaceHolderService.getUsersJson()
            .then((dataUsers)=> {

                if(!dataUsers || dataUsers.length === 0) resolve([]);

                const users = dataUsers.map((user) => {
                    return { 
                        name: user.name || null , 
                        email: user.website || null ,
                        company: (user.company && user.company  ? user.company : null) ,
                    };
                }).sort((a, b) => (a.name > b.name ? 1 : -1));

                return resolve(users);
            })
            .catch(error => reject(error));
        });
    }

    getUserWebsites(){
        return new Promise((resolve, reject) => {
            this.jsonPlaceHolderService.getUsersJson()
            .then((dataUsers) => {

                if(!dataUsers || dataUsers.length === 0) resolve([]);

                const websites = dataUsers.map((user) => {
                    return { user_id: user.id || null , website: user.website || null };
                })

                return resolve(websites);
            })
            .catch(error => reject(error));
            
        });
    }

    getUserSuitAddress(){
        return new Promise((resolve, reject) => {
            this.jsonPlaceHolderService.getUsersJson()
            .then((dataUsers) => {

                if(!dataUsers || dataUsers.length === 0) resolve([]);

                const users = dataUsers.filter(user => user.address && user.address.suite.toLowerCase().indexOf('suite') >= 0);

                return resolve(users);
            })
            .catch(error => reject(error));
        });
    }

   
}

module.exports = Business;





