'use strict';

const HandleResponse = require('../utils/handle_response');
const Business = require('./business');
const ElasticSearch = require('./db-adapters/elasticsearch/');

class Controller {
    constructor() {
        this.business  = new Business();
        this.handleResponse = new HandleResponse();
        this.elasticSearch = new ElasticSearch;
    }

    async index(req, res, next){
        return res.send('Hello World !');
    }

    websites(req,res,next){
        return new Promise((resolve, reject) => {

            this.business.getUserWebsites()
            .then((websites) => {
                res.json(websites);
                this.elasticSearch.log({success:true,'description':'/websites',});
            })
            .catch((error) => {
                console.log('error ',error);
                this.elasticSearch.log({success:false,'description':'/websites',message:JSON.stringify(error)});
                res.status(500).send(error);
            })
        });
    }

    getUsers(req,res,next){
        return new Promise((resolve, reject) => {
            this.business.getUsers()
            .then((users) => {
                res.json(users);
                this.elasticSearch.log({success:true,'description':'/users'});
            })
            .catch((error) => {
                console.log('error ',error);
                this.elasticSearch.log({success:false,'description':'/users',message:JSON.stringify(error)});
                res.status(500).send(error);
            })
        });
    }

    getUserSuitAddress(req,res,next){
        return new Promise((resolve, reject) => {
            this.business.getUserSuitAddress()
            .then((users) => {
                res.json(users);
                this.elasticSearch.log({success:true,'description':'/suits'});
            })
            .catch((error) => {
                console.log('error ',error);
                this.elasticSearch.log({success:false,'description':'/suits',message:JSON.stringify(error)});
                res.status(500).send(error);
            })
        });
    }

}

module.exports = Controller;
