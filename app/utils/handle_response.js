'use strict';

const _ = require('lodash');

class HandleResponse {
  
  forbidden(message) {
      let errorMessage = message || 'Forbidden';
      return this._errorReponse(403, errorMessage);
  }

  notFound(message) {
      let errorMessage = message || 'Resource not found';
      return this._errorReponse(404, errorMessage);
  }

  unauthorized(message) {
      let errorMessage = message || 'Missing token or invalid token';
      return this._errorReponse(401, errorMessage);
  }

  unprocessableEntity(message) {
      let errorMessage = message || 'Invalid credentials or missing parameters';
      return this._errorReponse(422, errorMessage);
  }

  handleError(error) {
      let message = error.message || 'Undefined error';

      let errorList = _.get(error, 'errors');

      if (_.isEmpty(errorList) === false) {
          let errorMessageList = _.map(errorList, errorItem => {
              return errorItem.message || 'Undefined error';
          });

          message = errorMessageList.toString();
      }

      let nameStatusMap = {
          'SequelizeUniqueConstraintError': 409
      };

      let status = nameStatusMap[error.name];

      return this._errorReponse(status, message);
  }

  _errorReponse(status, message) {
      let error = new Error();
      error.status = status || 400;
      error.message = message || 'Bad request';
      return error;
  }

  success(data, message = 'Successful', additionalData = {}) {
    data.status = 200;
    const defaultDataResponse = {
        data,
        message: message
    };

    const dataResponse = _.merge({}, defaultDataResponse,  additionalData);

    return dataResponse;
  }
  
}

module.exports = HandleResponse;
