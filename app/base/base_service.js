const _ = require('lodash');
const requestPromise = require('request-promise');

class BaseService {

    constructor(){
        this.requestPromise = requestPromise;
    }

    formatOptionsRequest({ headers, url, body }) {
        return {
            url,
            headers,
            json: body,
        };
    }

    hasError(error, response) {
        return error ||
        response.statusCode === 502 ||
        response.statusCode === 500 ||
        response.statusCode > 400;
    }

    isValid(response) {
        return response.statusCode === 200 ||
        response.statusCode === 201;
    }

    formatError(response) {
        const error = new Error();
        error.status = response.statusCode || 400;
        error.message = _.get(response, 'error.message') || 'Bad request';

        return error;
    }

    formatResponse(response) {
        let responseBody = response;

        if (!_.isObject(response)) {
            responseBody = JSON.parse(response);
        }

        return responseBody;
    }
}

module.exports = BaseService;
