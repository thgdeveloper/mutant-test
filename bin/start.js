'use strict';

const app = require('../app');

app.set('port',8080);

const server = app.listen(app.get('port'), () => {
console.log('------------------ RUN APPLICATION ------------------');
console.log('PORT: ', server.address().port);
});

