#!/bin/bash

until curl 'http://172.17.0.2:9200' 2> /dev/null; do
  echo "Waiting for elasticsearch..."
  sleep 1; 
done

# Start your server
npm start -p "8080"